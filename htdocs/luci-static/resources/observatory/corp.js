// CORP - Chunked Object Rebuilding Parser
// Parses strings that look like JSON, avoiding use of eval().
// Can be fed chunks of JSON; don't need to gather up a full string.
// Acts as a fallback when browser doesn't provide JSON.parse() and
// using eval() might be unacceptable.
// Copycenter 2022 Serena's Copycat
// @license magnet:?xt=urn:btih:90dc5c0be029de84e523b9b3922520e79e0e6f08&dn=cc0.txt CC0-1.0
// License: CC0
/* To the extent possible under law, the author has dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide.  This software is distributed without any warranty and
 * is published from: United States.
 * See <http://creativecommons.org/publicdomain/zero/1.0/>.
 * Feel free to mangle in any way, including hiding malicious instructions
 * and removing information related to license and/or author, but then
 * don't expect people to blame the author for undesired behavior though.
 * Note that no warranties of any kind are made regardless,
 * but you have the right to blame the author. XP
 */

// force CORP to load if JSON and CORP parse are the same, and CORP only has parse
if (((function () {
		/* check for a global JSON, avoiding reference to window */
		try {
			return JSON.parse === CORP.parse;
		} catch (e) {
			return false;
		}
	})() && (function () {
		for (var k in CORP) {
			if ("parse" != k) {
				// found something other than parse
				return false;
			}
		}
		// override condition met, force load
		return true;
	})()) || ((function () {
			try {	// might error trying to access undefined global property
				return null == CORP;
			} catch (e) {
				return true;
			}
		})() &&
		(function () {
			// otherwise, check for JSON functionality and skip loading if it's working
			function tryParse(json) {
				try {
					return JSON.parse(json);
				} catch (e) {
					return null;
				}
			}
			function testParse() {
				var result = tryParse("{\"test\": [ true ]}");
				return result && result.test && 0 < result.test.length && true === result.test[0];
			}
			return !testParse();
		})())) {
	CORP = new (function () {
		// work with prior CORP object before it gets clobbered
		this.linkParse = (function () {
				try {
					return JSON.parse === CORP.parse;
				} catch (e) {
					return false;
				}
			})();
		// a buffered stream
		function MovingStringWindow() {
			var _position = 0, _string = "", _stringCur = 0;
			// get absolute (based on beginning of stream) window position
			this.getWindowAt = function () {
				return _position;
			};
			// get absolute cursor position
			this.getCursorAt = function () {
				return _position + _stringCur;
			};
			this.getCursorInWindow = function () {
				return _stringCur;
			};
			this.append = function (str) {
				_string = (0 < _string.length) ? _string + str : str;
			};
			// set cursor position relative to beginning of window
			this.moveTo = function (cur) {
				_stringCur = cur;
			};
			// moves (cuts beginning of) window by count
			this.shift = function (count) {
				if (null == count) {
					count = _stringCur;
				} else if (Number.isNaN(count) || count < 0) {
					return;
				}
				_position += (null != count) ? count : (count = _stringCur);	
				_string = _string.substring(count);
			};
			function withRegex(regex, from, fn) {
				from = ("number" != typeof(from)) ? _stringCur :
					(0 < from) ? _string.length + from : from;
				if (regex.global) {
					var oldLastIndex = regex.lastIndex;
					try {
						regex.lastIndex = from;
						var result = fn(regex);
						regex.lastIndex = oldLastIndex;
						return result;
					} catch (e) {
						regex.lastIndex = oldLastIndex;
						throw e;
					}
				} else {
					(regex = new RegExp(regex.source, "g")).lastIndex = from;
					return fn(regex);
				}
			}
			// returns match relative to beginning of window
			this.searchFrom = function(regex, from) {
				// String.prototype.search returns first match, regardless of last index
				var match = this.matchFrom(regex, from);
				return match == null ? -1 : match.index;
			};
			this.matchFrom = function (regex, from) {
				return withRegex(regex, from, function (regex) {
					return regex.exec(_string);
				});
			};
			this.matchStartingAt = function(regex, at) {
				return (0 == (("number" == typeof(at)) ? at : (at = _stringCur))) ? this.matchFrom(regex, at)
					: withRegex(regex, 0, function (regex) {
						// adjust start of string so ^ anchor hits 'at' position
						var result = regex.exec(_string.substring(at));
						result.input = _string;
						result.index += at;
						return result;
					});
			};
			this.extract = function (from, to) {
				return _string.substring(from, to);
			};
			this.windowLength = function () {
				return _string.length;
			};
		}
		MovingStringWindow.prototype.movePastMatchEnd = function(match) {
			this.moveTo(match.index + match[0].length);
		};
		MovingStringWindow.prototype.isMatchEndClear = function(match) {
			return match.index + match[0].length < this.windowLength();
		};
		function ChunkParser() {
			/* Normally a full parse runs through function call tree in a single call.
			 * This simulates the function call stack, returning early at end of input,
			 * then resuming at the top of stack with new chunks.
			 * Each stack element is a "mode" that handles a specific kind of [complex] value.
			 * ("complex" meaning difficult to handle by regex alone.)
			 * Modes can have submodes that handles a specific syntax element.
			 */
			var _rgxNonBlank = /\S/;
			/* single quoting is illegal in JSON...  but valid in JS. */
			var _rgxStartAtValue = /^(?:[\[\{"']|\d|false|null|true)/;
			var _rgxStringSpecial = /['"]|\\[^ux]|\\x[^\\]{0,2}|\\u[^\\]{0,4}|[\u0000-\u001a]/;
			var _rgxStartAtHex = /^[0-9a-fA-F]*/;
			var _rgxStartAtNumber = /^-?(?:0|[1-9]\d*)(?:\.\d+)?(?:[eE][-\+]?\d+)?/;
			var _stream = new MovingStringWindow();
			var _resumeMode = null;	// top of emulated stack
			var _lax = false;	// flag indicating input doesn't follow strict JSON
			function tagLax(reason) {
				_lax = true;
			}
			var _isEndOfStream = false;
			this.setEndOfStream = function (isEnd) {
				_isEndOfStream = isEnd;
			};
			function ModeContext(resumeUpperFn) {
				var _resume = resumeUpperFn;
				var _position = _stream.getCursorAt();
				this.resumeUpper = function () {
					_resume();
				};
			};
			function SubmodeManager(context, modes) {
				var _context = new ModeContext(context);
				var _submode = modes.start;
				this.setMode = function(mode) {
					_submode = modes[mode];
				};
				this.resume = function (thisObj, data) {
					_submode.call(thisObj, this, _context, data);
				};
			};
			function ObjectMode(parentContext) {
				var _submode = new SubmodeManager(parentContext, this.modes);
				var build = this.build = {};
				var _keyName;
				var _access = {
					setKeyName: function (name) {
						_keyName = name;
					},
					completeEntry: function (value) {
						build[_keyName] = value;
					}
				};
				this.resume = function () {
					_submode.resume(this, _access);
				};
				_stream.searchFrom(_rgxNonBlank, _stream.getCursorInWindow() + 1);
			};
			ObjectMode.prototype.modes = {
				start: function (modes, context, access) {
					// move past the object-start brace
					_stream.moveTo(_stream.getCursorInWindow() + 1);
					modes.setMode("key");
					this.resume();
				},
				key: function (modes, context, access) {
					var thisMode = this;
					var nextMode = _resumeMode = new StringMode(function () {
						access.setKeyName(nextMode.getParsed());
						modes.setMode("split");
						thisMode.resume();
					});
					_resumeMode.resume();
				},
				split: function (modes, context, access) {
					var match = _stream.matchFrom(_rgxNonBlank);
					if (null == match) {
						_stream.shift();
					} else {
						if (":" != match[0]) {
							throw ("Unexpected key-value separator: " + match);
						} else {
							_stream.movePastMatchEnd(match);
							modes.setMode("value");
							this.resume();
						}
					}
				},
				value: function (modes, context, access) {
					var thisMode = this;
					var nextMode = _resumeMode = new ValueMode(function () {
						access.completeEntry(nextMode.getParsed());
						modes.setMode("next");
						thisMode.resume();
					});
					_resumeMode.resume();
				},
				next: function (modes, context, access) {
					var match = _stream.matchFrom(_rgxNonBlank);
					if (null == match) {
						_stream.shift();
					} else if ("," == match[0]) {
						_stream.movePastMatchEnd(match);
						modes.setMode("key");
						this.resume();
					} else if ("}" == match[0]) {
						_stream.movePastMatchEnd(match);
						context.resumeUpper();
					} else {
						throw "Unexpected token in object";
					}
				},
			};
			ObjectMode.prototype.getParsed = function () {
				return this.build;
			};
			function ArrayMode(parentContext) {
				var _submode = new SubmodeManager(parentContext, this.modes);
				this.build = [];
				this.resume = function () {
					_submode.resume(this);
				};
			};
			ArrayMode.prototype.modes = {
				start: function (modes, context, access) {
					// move past the array-start bracket
					_stream.moveTo(_stream.getCursorInWindow() + 1);
					modes.setMode("value");
					this.resume();
				},
				value: function (modes, context, access) {
					var thisMode = this;
					var nextMode = _resumeMode = new ValueMode(function () {
						_resumeMode = thisMode;
						thisMode.build.push(nextMode.getParsed());
						modes.setMode("next");
						thisMode.resume();
					});
					_resumeMode.resume();
				},
				next: function (modes, context, access) {
					var match = _stream.matchFrom(_rgxNonBlank);
					if (null == match) {
						_stream.shift();
					} else if ("," == match[0]) {
						_stream.movePastMatchEnd(match);
						modes.setMode("value");
						this.resume();
					} else if ("]" == match[0]) {
						_stream.movePastMatchEnd(match);
						context.resumeUpper();
					} else {
						throw "Unexpected token in array";
					}
				},
			}
			ArrayMode.prototype.getParsed = function () {
				return this.build;
			};
			function StringMode(parentContext) {
				var _submode = new SubmodeManager(parentContext, this.modes);
				this.build = [];
				this.resume = function () {
					_submode.resume(this, this.build);
				};
			};
			StringMode.prototype.build = null;
			StringMode.prototype.quote = null;
			function isStringQuote(str) {
				return "\"" == str || "'" == str;
			}
			function getCharCodeEscape(codeStr, expectedLength) {
				return codeStr.replace(_rgxStartAtHex, function (match) {
					if (0 == match.length) {
						return match;	// don't add a null char for empty code
					} else if (expectedLength > match.length) {
						tagLax("Unexpected hex code length (" + match.length + " of " + expectedLength + ")");
					}
					return String.fromCharCode(Number.parseInt(match, 16));
				});
			}
			function buildEscape(matched, build) {
				switch (matched[1]) {
					case "0": build.push("\0"); tagLax("null char in string"); break;
					case "b": build.push("\b"); break;
					case "f": build.push("\f"); break;
					case "n": build.push("\n"); break;
					case "r": build.push("\r"); break;
					case "t": build.push("\t"); break;
					case "u":
						build.push(getCharCode(matched.substring(2), 4));
						break;
					case "v": build.push("\v"); tagLax("vertical form feed in string"); break;
					case "x":
						tagLax("hex char escape in string");
						build.push(getCharCode(matched.substring(2), 2));
						break;
					default:
						var escaped = matched.substring(1);
						if ("\"" != escaped && "\\" != escaped && "/" != escaped) {
							tagLax("escaped " + escaped);
						}
						build.push(escaped);
						break;
				}
			}
			StringMode.prototype.modes = {
				start: function (modes, context, build) {
					var quoteMatch = _stream.matchStartingAt(_rgxStartAtValue);
					if (!quoteMatch) {
						throw ("Unexpected start of string at " + _stream.getCursorAt + ".");
					} else {
						this.quote = quoteMatch[0];
						if (!isStringQuote(this.quote)) {
							throw ("Unexpected quote " + this.quote);
						} else if (this.quote != "\"") {
							tagLax("String quote with " + this.quote);
						}
						_stream.movePastMatchEnd(quoteMatch);
						modes.setMode("build");
						this.resume();
					}
				},
				build: function (modes, context, build) {
					var match;
					while (null != (match = _stream.matchFrom(_rgxStringSpecial))) {
						var literalStart = _stream.getCursorInWindow(), literalEnd = match.index;
						if (literalStart < literalEnd) {
							build.push(_stream.extract(literalStart, literalEnd));
						}
						_stream.movePastMatchEnd(match);
						var matched = match[0];
						if (matched == this.quote) {
							// end-of-string
							modes.setMode("start");
							context.resumeUpper();
							return;
						} else if (isStringQuote(matched)) {
							// normal quote, not end-of-string quote
							build.push(matched);
						} else if (matched[0] == "\\") {
							buildEscape(matched, build);
						} else if (1 == matched.length) {
							tagLax("unexpected char at " + (_stream.getWindowAt + literalStart));
							build.push(matched);
						}
					}
					build.push(_stream.extract(_stream.getCursorAt));
					_stream.shift(_stream.windowLength);
				}
			};
			StringMode.prototype.getParsed = function () {
				return this.value || (this.value = this.build.join(""));
			};
			/**
			 * Processes a single value (literal value, object or array).
			 */
			function ValueMode(parentContext) {
				var context = new ModeContext(parentContext);
				var _result;
				var _thisMode = this;
				function newMode(modeCnstr) {
					var nextMode = _resumeMode = new modeCnstr(function () {
						_resumeMode = _thisMode;
						_result = nextMode.getParsed();
						context.resumeUpper();
					});
					_resumeMode.resume();
				}
				this.resume = function() {
					var match = _stream.searchFrom(_rgxNonBlank);
					if (0 > match) {
						return;
					} else {
						_stream.moveTo(match);
					}
					var startMatch = _stream.matchStartingAt(_rgxStartAtValue), startStr;
					if (null == startMatch) {
						throw ("Unexpected value start at" + _stream.getCursorAt());
					} else if ("{" === (startStr = startMatch[0])) {
						newMode(ObjectMode);
					} else if ("[" === startStr) {
						newMode(ArrayMode);
					} else if (isStringQuote(startStr)) {
						newMode(StringMode);
					} else if ("null" === startStr) {
						_result = null;
						_stream.movePastMatchEnd(startMatch);
						context.resumeUpper();
					} else if ("true" === startStr) {
						_result = true;
						_stream.movePastMatchEnd(startMatch);
						context.resumeUpper();
					} else if ("false" === startStr) {
						_result = false;
						_stream.movePastMatchEnd(startMatch);
						context.resumeUpper();
					} else if (_rgxStartAtNumber.exec(startStr)) {
						var numberMatch = _stream.matchStartingAt(_rgxStartAtNumber);
						if (_isEndOfStream || _stream.isMatchEndClear(numberMatch)) {
							_result = (Number.parseFloat || parseFloat)(numberMatch[0]);
							_stream.movePastMatchEnd(numberMatch);
							context.resumeUpper();
						} // else could have cut off mid-number by chunk end
					} else {
						throw ("BUG: unhandled starting token " + startStr + " at " + _stream.getCursorAt());
					}
				};
				this.getParsed = function () {
					return _result;
				};
			}
			var _result, _resultSerial = 0;
			function RootMode() {
				this.resume = function () {
					var thisMode = this;
					var elementMode = _resumeMode = new ValueMode(function () {
						_resumeMode = thisMode;
						_result = elementMode.getParsed();
						++_resultSerial;
					});
					_resumeMode.resume();
				};
			};
			this.parseChunk = function (json) {
				_stream.append(json);
				var oldSerial = _resultSerial;
				(_resumeMode || (_resumeMode = new RootMode())).resume();
				_stream.shift();
				return oldSerial < _resultSerial;
			}
			this.getParsed = function () {
				return _result;
			}
		}
		this.newParser = function () {
			return new ChunkParser();
		};
		this.parse = function (json) {
			var parser = new ChunkParser();
			parser.setEndOfStream(true);
			return parser.parseChunk(json) ? parser.getParsed() : null;
		}
	})();
	if (!(function () {
				try {
					return JSON || (JSON = {});
				} catch (e) {
					return (JSON = {});
				}
			})().parse) {
		CORP.linkParse = true;
	}
	if (CORP.linkParse) {
		JSON.parse = CORP.parse;
	}
	delete CORP.linkParse;
}
/* @license-end */
